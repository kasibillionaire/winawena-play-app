import org.junit.jupiter.api.Test
import za.co.winawena.Lotto
import za.co.winawena.PowerBall
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class LottoTest {
    private val lotto = Lotto()
    private val powerBall = PowerBall()
    private var generatedList = arrayListOf<Int>()

    @Test
    fun shouldReturnListOfNumbers() {
        generatedList = lotto.numberGenerator(50)
        assertNotNull(generatedList)
        assertEquals(50, generatedList.size)
    }

    @Test
    fun shouldReturnShuffledList() {
        generatedList = lotto.numberGenerator(50)
        val shuffledList = lotto.numberShuffler(generatedList)
        assertNotNull(generatedList)
        assertNotNull(shuffledList)
    }

    @Test
    fun shouldPrint6Numbers() {
        generatedList = lotto.numberGenerator(52)
        val numberShuffler = lotto.numberShuffler(generatedList)
        val numberPrinter = lotto.numberPrinter(numberShuffler, 5)
        assertTrue(numberPrinter.isNotBlank())
        println(numberPrinter)
    }

    @Test
    fun shouldReturn5NumbersPlusPowerball() {
        generatedList = powerBall.numberGenerator(50)
        val numberShuffler = powerBall.numberShuffler(generatedList)
        val numberPrinter = powerBall.numberPrinter(numberShuffler,0)
        assertTrue(numberPrinter.isNotBlank())
        println(numberPrinter)
    }

    @Test
    fun shouldReturn4HotAndColdNumbers() {
        val hotAndColdList = arrayListOf(35, 30, 29, 36, 11, 19, 17, 48, 47, 40, 9, 24, 15, 18)
        val shuffler = lotto.numberShuffler(hotAndColdList)
        val printer = lotto.numberPrinter(shuffler, 4)
        assertTrue(printer.isNotBlank())
        println(printer)
    }
}