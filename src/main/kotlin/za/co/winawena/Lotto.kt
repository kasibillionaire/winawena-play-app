package za.co.winawena

import java.util.*

class Lotto : Lottery {

    override fun numberPrinter(shuffledList: ArrayList<Int>, loopLimit: Int): String {
        var lottoNumbers = String()
        for (i in 0 until loopLimit) {
            lottoNumbers += shuffledList[i].toString() + "-"
        }
        return lottoNumbers.trim('-')
    }
}