package za.co.winawena

class PowerBall : Lottery {

    override fun numberPrinter(shuffledList: ArrayList<Int>, loopLimit: Int): String {
        var powerNumbers = String()
        for (i in 0..4) {
            powerNumbers += shuffledList[i].toString() + "-"
        }
        return powerNumbers.trim('-') + " PowerBall " + numberShuffler(numberGenerator(20))[0].toString()
    }
}