package za.co.winawena

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WinaWenaPlay {

    fun main(args: Array<String>) {
        runApplication<WinaWenaPlay>(*args)
    }
}