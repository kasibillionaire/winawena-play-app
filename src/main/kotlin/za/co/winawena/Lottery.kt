package za.co.winawena

import java.util.*
import kotlin.collections.ArrayList

interface Lottery {
    fun numberGenerator(maxNo: Int): ArrayList<Int> {
        val numberList = arrayListOf<Int>()
        for (i in 1..maxNo) {
            numberList.add(i)
        }
        return numberList
    }

    fun numberShuffler(numberList: ArrayList<Int>): ArrayList<Int> {
        numberList.shuffle(Random())
        return numberList
    }

    fun numberPrinter(shuffledList: ArrayList<Int>, loopLimit: Int): String
}